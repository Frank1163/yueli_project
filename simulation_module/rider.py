from location import Location

"""
The rider module contains the Rider class. It also contains
constants that represent the status of the rider.

=== Constants ===
@type WAITING: str
    A constant used for the waiting rider status.
@type CANCELLED: str
    A constant used for the cancelled rider status.
@type SATISFIED: str
    A constant used for the satisfied rider status
"""

WAITING = "waiting"
CANCELLED = "cancelled"
SATISFIED = "satisfied"


class Rider:
    # TODO
    """
    A rider with identifier, origin, destination, status and patience.

    === Attribute ===
    @param str id: a unique id for a ride
    @param Location origin: the place where rider is at present
    @param Location destination: the place where rider wants to go
    @param str status: the status of rider at present
    @param int patience: the number of minutes the rider will wait
    """

    def __init__(self, identifier, origin, destination, patience):
        """
        Create a new rider with identifier, origin, destination, status \
        and patience.

        @param str identifier: a unique id for a ride
        @param Location origin: the place where rider is at present
        @param Location destination: the place where rider wants to go
        @param int patience: the number of minutes the rider will wait
        @rtype: None
        """
        self.id = identifier
        self.origin = origin
        self.destination = destination
        self.status = WAITING
        self.patience = patience

    def __eq__(self, other):
        """
        Return whether Rider self is equivalent to other.

        @param Rider self: this Rider object
        @param Rider|Any other: object to be compared to
        @rtype: bool

        >>> r1 = Rider('a', Location(1, 2), Location(4, 5), 10)
        >>> r2 = Rider('b', Location(1, 2), Location(4, 5), 15)
        >>> r1 == r2
        False
        """
        return (type(self) == type(other) and
                self.id == other.id and
                self.origin == other.origin and
                self.destination == other.destination and
                self.patience == other.patience)

    def __str__(self):
        """
        Return an informative string describing self.

        @param Rider self: this Rider object
        @rtype: str

        >>> r = Rider('a', Location(1, 2), Location(4, 5), 10)
        >>> r.__str__()
        'a'
        """
        return '{}'.format(self.id)
