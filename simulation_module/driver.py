from location import Location, manhattan_distance
from rider import Rider


class Driver:
    """A driver for a ride-sharing service.

    === Attributes ===
    @type id: str
        A unique identifier for the driver.
    @type location: Location
        The current location of the driver.
    @type is_idle: bool
        A property that is True if the driver is idle and False otherwise.
    """

    def __init__(self, identifier, location, speed):
        """Initialize a Driver.

        @type self: Driver
        @type identifier: str
        @type location: Location
        @type speed: int
        @rtype: None
        """
        # TODO
        self.id, self.location, self.speed = identifier, location, speed
        self.is_idle = True
        self.destination = None

    def __str__(self):
        """Return a string representation.

        @type self: Driver
        @rtype: str

        >>> d = Driver('b', Location(2, 3), 1)
        >>> print(d)
        b
        """
        # TODO
        return '{}'.format(self.id)

    def __eq__(self, other):
        """Return True if self equals other, and false otherwise.

        @type self: Driver
        @rtype: bool

        >>> d1 = Driver('b', Location(2, 3), 1)
        >>> d2 = Driver('c', Location(2, 3), 1)
        >>> d1 == d2
        False
        """
        # TODO
        return (type(self) == type(other) and
                self.id == other.id and
                self.location == other.location and
                self.speed == other.speed)

    def get_travel_time(self, destination):
        """Return the time it will take to arrive at the destination,
        rounded to the nearest integer.

        @type self: Driver
        @type destination: Location
        @rtype: int

        >>> d = Driver('b', Location(2, 3), 3)
        >>> d.get_travel_time(Location(9, 9))
        4
        """
        # TODO
        distance = manhattan_distance(self.location, destination)
        time = distance / self.speed
        if time - int(time) < 0.5:
            return int(time)
        if time - int(time) >= 0.5:
            time += 1
            return int(time)

    def start_drive(self, location):
        """Start driving to the location and return the time the drive will take.

        @type self: Driver
        @type location: Location
        @rtype: int

        >>> d = Driver('b', Location(2, 3), 2)
        >>> d.start_drive(Location(9, 9))
        7
        """
        # TODO
        self.is_idle = False
        self.destination = location
        time = self.get_travel_time(self.destination)
        return time

    def end_drive(self):
        """End the drive and arrive at the destination.

        Precondition: self.destination is not None.

        @type self: Driver
        @rtype: None
        """
        # TODO
        self.location = self.destination

    def start_ride(self, rider):
        """Start a ride and return the time the ride will take.

        @type self: Driver
        @type rider: Rider
        @rtype: int

        >>> d = Driver('b', Location(1, 2), 2)
        >>> r = Rider('a', Location(1, 2), Location(4, 5), 10)
        >>> d.start_ride(r)
        3
        """
        # TODO
        self.location = rider.origin
        self.destination = rider.destination
        time = self.get_travel_time(self.destination)
        return time

    def end_ride(self):
        """End the current ride, and arrive at the rider's destination.

        Precondition: The driver has a rider.
        Precondition: self.destination is not None.

        @type self: Driver
        @rtype: None
        """
        # TODO
        self.location = self.destination
        self.destination = None
        self.is_idle = True
