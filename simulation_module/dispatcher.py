# import class Location in order to pass doctest.
from location import Location
from driver import Driver
from rider import Rider


class Dispatcher:
    """A dispatcher fulfills requests from riders and drivers for a
    ride-sharing service.

    When a rider requests a driver, the dispatcher assigns a driver to the
    rider. If no driver is available, the rider is placed on a waiting
    list for the next available driver. A rider that has not yet been
    picked up by a driver may cancel their request.

    When a driver requests a rider, the dispatcher assigns a rider from
    the waiting list to the driver. If there is no rider on the waiting list
    the dispatcher does nothing. Once a driver requests a rider, the driver
    is registered with the dispatcher, and will be used to fulfill future
    rider requests.
    """

    def __init__(self):
        """Initialize a Dispatcher.

        @type self: Dispatcher
        @rtype: None
        """
        # TODO
        self._wl = []
        self._driving_fleet = []

    def __str__(self):
        """Return a string representation.

        @type self: Dispatcher
        @rtype: str

        >>> d = Dispatcher()
        >>> r = Rider('a', Location(1, 1), Location(2,2), 10)
        >>> d.request_driver(r)
        >>> print(d)
        Riders waiting for a drive:a
        Drivers request a ride:
        """
        # TODO
        s = "Riders waiting for a drive:"
        if len(self._wl) != 0:
            for rider in self._wl:
                s += rider.id + ', '
            s = s[0:-2] + '\n' + 'Drivers request a ride:'
        else:
            s += '\n' + 'Drivers request a ride:'
        if len(self._driving_fleet) != 0:
            for driver in self._driving_fleet:
                s += driver.id + ', '
            s = s[0:-2]
        return s

    def request_driver(self, rider):
        """Return a driver for the rider, or None if no driver is available.

        Add the rider to the waiting list if there is no available driver.

        @type self: Dispatcher
        @type rider: Rider
        @rtype: Driver | None

        >>> d = Dispatcher()
        >>> rider1 = Rider('a', Location(1, 1), Location(2,2), 10)
        >>> driver1 = Driver('b', Location(2, 1), 1)
        >>> d.request_rider(driver1)
        >>> driver = d.request_driver(rider1)
        >>> print(driver.id)
        b

        """
        # TODO
        if len(self._driving_fleet) == 0:
            self._wl.append(rider)
        elif len(self._driving_fleet) != 0:
            # Create a new list to store the travel times.
            l = []
            for i in range(len(self._driving_fleet)):
                l.append(self._driving_fleet[i].start_drive(rider.origin))
            index = l.index(min(l))
            assigned_driver = self._driving_fleet[index]
            self._driving_fleet.remove(assigned_driver)
            return assigned_driver

    def request_rider(self, driver):
        """Return a rider for the driver, or None if no rider is available.

        If this is a new driver, register the driver for future rider requests.

        @type self: Dispatcher
        @type driver: Driver
        @rtype: Rider | None

        >>> d = Dispatcher()
        >>> rider1 = Rider('a', Location(1, 1), Location(2,2), 10)
        >>> driver1 = Driver('b', Location(2, 1), 1)
        >>> d.request_driver(rider1)
        >>> rider = d.request_rider(driver1)
        >>> print(rider.id)
        a
        """
        # TODO
        self._driving_fleet.append(driver)
        if len(self._wl) != 0:
            longest_waiting = self._wl[0]
            self._driving_fleet.remove(driver)
            if longest_waiting in self._wl:
                self._wl.remove(longest_waiting)
            return longest_waiting

    def cancel_ride(self, rider):
        """Cancel the ride for rider.

        @type self: Dispatcher
        @type rider: Rider
        @rtype: None
        """
        # TODO
        if rider in self._wl:
            self._wl.remove(rider)
