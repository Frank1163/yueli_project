class Location:
    """ A Location class.

    === Attribute ===
    @param int row: a int row to represent the location from the bottom edge of
    the grid.
    @param int column: a int column to represent the location from the left of
    the grid
    """

    def __init__(self, row, column):
        """Initialize a location.

        @type self: Location
        @type column: int
        @type row: int
        @rtype: None
        """
        # TODO
        self.row, self.column = row, column

    def __str__(self):
        """Return a string representation.

        @rtype: str

        >>> l = Location(2, 3)
        >>> print(l)
        2,3
        """
        # TODO
        return '{},{}'.format(self.row, self.column)

    def __eq__(self, other):
        """Return True if self equals other, and false otherwise.

        @rtype: bool

        >>> l1 = Location(3, 4)
        >>> l2 = Location(3, 5)
        >>> l1 == l2
        False
        """
        # TODO
        return (type(self) == type(other) and
                self.row == other.row and self.column == other.column)


def manhattan_distance(origin, destination):
    """Return the Manhattan distance between the origin and the destination.

    @type origin: Location
    @type destination: Location
    @rtype: int

    >>> l1 = Location(1, 2)
    >>> l2 = Location(5, 4)
    >>> manhattan_distance(l1, l2)
    6
    """
    # TODO
    distance = abs(origin.row - destination.row) + abs(origin.column -
                                                       destination.column)
    return distance


def deserialize_location(location_str):
    """Deserialize a location.

    @type location_str: str
        A location in the format 'row,col'
    @rtype: Location

    >>> l = deserialize_location('2, 3')
    >>> print(l)
    2,3
    """
    # TODO
    location_list = location_str.split(',')
    row = int(location_list[0])
    column = int(location_list[-1])
    location = Location(row, column)
    return location
