"""Simulation Events

This file should contain all of the classes necessary to model the different
kinds of events in the simulation.
"""
from rider import Rider, WAITING, CANCELLED, SATISFIED
from dispatcher import Dispatcher
from driver import Driver
from location import Location, deserialize_location
from monitor import Monitor, RIDER, DRIVER, REQUEST, CANCEL, PICKUP, DROPOFF


class Event:
    """An event.

    Events have an ordering that is based on the event timestamp: Events with
    older timestamps are less than those with newer timestamps.

    This class is abstract; subclasses must implement do().

    You may, if you wish, change the API of this class to add
    extra public methods or attributes. Make sure that anything
    you add makes sense for ALL events, and not just a particular
    event type.

    Document any such changes carefully!

    === Attributes ===
    @type timestamp: int
        A timestamp for this event.
    """

    def __init__(self, timestamp):
        """Initialize an Event with a given timestamp.

        @type self: Event
        @type timestamp: int
            A timestamp for this event.
            Precondition: must be a non-negative integer.
        @rtype: None

        >>> Event(7).timestamp
        7
        """
        self.timestamp = timestamp

    # The following six 'magic methods' are overridden to allow for easy
    # comparison of Event instances. All comparisons simply perform the
    # same comparison on the 'timestamp' attribute of the two events.
    def __eq__(self, other):
        """Return True iff this Event is equal to <other>.

        Two events are equal iff they have the same timestamp.

        @type self: Event
        @type other: Event
        @rtype: bool

        >>> first = Event(1)
        >>> second = Event(2)
        >>> first == second
        False
        >>> second.timestamp = first.timestamp
        >>> first == second
        True
        """
        return self.timestamp == other.timestamp

    def __ne__(self, other):
        """Return True iff this Event is not equal to <other>.

        @type self: Event
        @type other: Event
        @rtype: bool

        >>> first = Event(1)
        >>> second = Event(2)
        >>> first != second
        True
        >>> second.timestamp = first.timestamp
        >>> first != second
        False
        """
        return not self == other

    def __lt__(self, other):
        """Return True iff this Event is less than <other>.

        @type self: Event
        @type other: Event
        @rtype: bool

        >>> first = Event(1)
        >>> second = Event(2)
        >>> first < second
        True
        >>> second < first
        False
        """
        return self.timestamp < other.timestamp

    def __le__(self, other):
        """Return True iff this Event is less than or equal to <other>.

        @type self: Event
        @type other: Event
        @rtype: bool

        >>> first = Event(1)
        >>> second = Event(2)
        >>> first <= first
        True
        >>> first <= second
        True
        >>> second <= first
        False
        """
        return self.timestamp <= other.timestamp

    def __gt__(self, other):
        """Return True iff this Event is greater than <other>.

        @type self: Event
        @type other: Event
        @rtype: bool

        >>> first = Event(1)
        >>> second = Event(2)
        >>> first > second
        False
        >>> second > first
        True
        """
        return not self <= other

    def __ge__(self, other):
        """Return True iff this Event is greater than or equal to <other>.

        @type self: Event
        @type other: Event
        @rtype: bool

        >>> first = Event(1)
        >>> second = Event(2)
        >>> first >= first
        True
        >>> first >= second
        False
        >>> second >= first
        True
        """
        return not self < other

    def __str__(self):
        """Return a string representation of this event.

        @type self: Event
        @rtype: str
        """
        raise NotImplementedError("Implemented in a subclass")

    def do(self, dispatcher, monitor):
        """Do this Event.

        Update the state of the simulation, using the dispatcher, and any
        attributes according to the meaning of the event.

        Notify the monitor of any activities that have occurred during the
        event.

        Return a list of new events spawned by this event (making sure the
        timestamps are correct).

        Note: the "business logic" of what actually happens should not be
        handled in any Event classes.

        @type self: Event
        @type dispatcher: Dispatcher
        @type monitor: Monitor
        @rtype: list[Event]
        """
        raise NotImplementedError("Implemented in a subclass")


class RiderRequest(Event):
    """A rider requests a driver.

    === Attributes ===
    @type rider: Rider
        The rider.
    """

    def __init__(self, timestamp, rider):
        """Initialize a RiderRequest event.

        @type self: RiderRequest
        @type rider: Rider
        @rtype: None
        """
        super().__init__(timestamp)
        self.rider = rider

    def do(self, dispatcher, monitor):
        """Assign the rider to a driver or add the rider to a waiting list.
        If the rider is assigned to a driver, the driver starts driving to
        the rider.

        Return a Cancellation event. If the rider is assigned to a driver,
        also return a Pickup event.

        @type self: RiderRequest
        @type dispatcher: Dispatcher
        @type monitor: Monitor
        @rtype: list[Event]

        >>> dispatcher = Dispatcher()
        >>> monitor = Monitor()
        >>> rider = Rider('a', Location(1, 2), Location(4, 5), 10)
        >>> r = RiderRequest(0, rider)
        >>> result = r.do(dispatcher, monitor)
        >>> len(result)
        1
        >>> type(result[0])
        <class 'event.Cancellation'>
        """
        monitor.notify(self.timestamp, RIDER, REQUEST,
                       self.rider.id, self.rider.origin)

        events = []
        driver = dispatcher.request_driver(self.rider)
        if driver is not None:
            travel_time = driver.start_drive(self.rider.origin)
            events.append(Pickup(self.timestamp + travel_time,
                                 self.rider, driver))
        events.append(Cancellation(self.timestamp + self.rider.patience,
                                   self.rider))
        return events

    def __str__(self):
        """Return a string representation of this event.

        @type self: RiderRequest
        @rtype: str

        >>> r = Rider('a', Location(2, 2), Location(2, 3), 13)
        >>> e = RiderRequest(3, r)
        >>> print(e)
        3 -- a: Request a driver
        """
        return "{} -- {}: Request a driver".format(self.timestamp, self.rider)


class DriverRequest(Event):
    """A driver requests a rider.

    === Attributes ===
    @type driver: Driver
        The driver.
    """

    def __init__(self, timestamp, driver):
        """Initialize a DriverRequest event.

        @type self: DriverRequest
        @type driver: Driver
        @rtype: None
        """
        super().__init__(timestamp)
        self.driver = driver

    def do(self, dispatcher, monitor):
        """Register the driver, if this is the first request, and
        assign a rider to the driver, if one is available.

        If a rider is available, return a Pickup event.

        @type self: DriverRequest
        @type dispatcher: Dispatcher
        @type monitor: Monitor
        @rtype: list[Event]

        >>> dispatcher = Dispatcher()
        >>> monitor = Monitor()
        >>> driver = Driver('b', Location(1, 2), 10)
        >>> r = DriverRequest(0, driver)
        >>> r.do(dispatcher, monitor)
        []
        """
        # Notify the monitor about the request.

        # Request a rider from the dispatcher.
        # If there is one available, the driver starts driving towards the
        # rider, and the method returns a Pickup event for when the driver
        # arrives at the riders location.
        # TODO
        monitor.notify(self.timestamp, DRIVER, REQUEST,
                       self.driver.id, self.driver.location)

        events = []
        rider = dispatcher.request_rider(self.driver)
        if rider is not None:
            travel_time = self.driver.start_drive(rider.origin)
            events.append(Pickup(self.timestamp + travel_time,
                                 rider, self.driver))
        return events

    def __str__(self):
        """Return a string representation of this event.

        @param DriverRequest self: this DriverRequest event.
        @rtype: str

        >>> d = Driver('b', Location(2, 2), 13)
        >>> e = DriverRequest(3, d)
        >>> print(e)
        3 -- b: Request a rider
        """
        return "{} -- {}: Request a rider".format(self.timestamp, self.driver)


class Cancellation(Event):
    # TODO
    """A rider cancel the current ride.

    === Attribute ===
    @param Rider rider: this Rider object
    """

    def __init__(self, timestamp, rider):
        """Initialize a Cancellation event.

        @param Cancellation self: this Cancellation event.
        @param Rider rider: the rider cancelling the current ride.
        @rtype: None
        """
        super().__init__(timestamp)
        self.rider = rider

    def do(self, dispatcher, monitor):
        """The rider cancels this ride, which is changed from a waiting rider to
        a cancelled rider, no more events will be scheduled, so return an empty
        list of Event.

        @param Cancellation self: Cancellation Event.
        @param  Dispatcher dispatcher: Dispatcher object.
        @param Monitor monitor: Monitor object
        @rtype: list[Event]

        >>> dispatcher = Dispatcher()
        >>> monitor = Monitor()
        >>> rider = Rider('a', Location(1, 2), Location(4, 5), 10)
        >>> r = Cancellation(0, rider)
        >>> r.do(dispatcher, monitor)
        []
        """
        events = []
        if self.rider.status != SATISFIED:
            monitor.notify(self.timestamp, RIDER, CANCEL, self.rider.id,
                           self.rider.origin)
            dispatcher.cancel_ride(self.rider)
            self.rider.status = CANCELLED

        return events

    def __str__(self):
        """Return a string representation of this event.

        @param Cancellation self: this Cancellation event.
        @rtype: str

        >>> r = Rider('a', Location(2, 2), Location(2, 3), 13)
        >>> c = Cancellation(13, r)
        >>> print(c)
        13 -- a: Cancel request
        """
        return "{} -- {}: Cancel request".format(self.timestamp, self.rider)


class Pickup(Event):
    # TODO
    """The driver arrive at the rider's location and pick up that rider.

    === Attribute ===
    @param Rider rider: the rider who is picked up by driver.
    @param Driver driver: the driver who picks up rider.
    """

    def __init__(self, timestamp, rider, driver):
        """Initialize a Pickup event.

        @param Pickup self: this Pickup event.
        @param Rider rider: the rider who is picked up by driver.
        @param Driver driver: the driver who picks up rider.
        @rtype: None
        """
        super().__init__(timestamp)
        self.rider, self.driver = rider, driver

    def do(self, dispatcher, monitor):
        """The driver arrives at rider's location.
        If rider is waiting, driver gives rider a ride and return a Droppoff
        event. Or return a DriverRequest event, if rider has cancelled.
        If rider has cancelled, return a DriverRequest event and the driver
        becomes idle for the moment.

        @param Pickup self: this Pickup event.
        @param  Dispatcher dispatcher: Dispatcher object.
        @param Monitor monitor: Monitor object
        @rtype: list[Event]

        >>> dispatcher = Dispatcher()
        >>> monitor = Monitor()
        >>> rider = Rider('a', Location(1, 2), Location(4, 5), 10)
        >>> driver = Driver('b', Location(1, 2), 2)
        >>> p = Pickup(0, rider, driver)
        >>> result = p.do(dispatcher, monitor)
        >>> len(result)
        1
        >>> type(result[0])
        <class 'event.Dropoff'>
        """
        self.driver.end_drive()
        events = []
        if self.rider.status == WAITING:
            # If rider was in wait list before assigned a driver, now remove
            #  that rider from wait list.
            monitor.notify(self.timestamp, DRIVER, PICKUP,
                           self.driver.id, self.driver.location)
            monitor.notify(self.timestamp, RIDER, PICKUP,
                           self.rider.id, self.rider.origin)
            travel_time = self.driver.start_ride(self.rider)
            events.append(Dropoff(self.timestamp + travel_time, self.driver,
                                  self.rider))
            self.rider.status = SATISFIED

        elif self.rider.status == CANCELLED:
            events.append(DriverRequest(self.timestamp, self.driver))
            self.driver.is_idle = True
            self.driver.destination = None
        return events

    def __str__(self):
        """Return a string representation of this event.

        @param Pickup self: this Pickup event.
        @rtype: str

        >>> r = Rider('a', Location(2, 2), Location(2, 3), 5)
        >>> d = Driver('b', Location(2, 2), 13)
        >>> p = Pickup(3, r, d)
        >>> print(p)
        3 -- b: Pickup a
        """
        return "{} -- {}: Pickup {}".format(self.timestamp, self.driver,
                                            self.rider)


class Dropoff(Event):
    # TODO
    """Driver arrives at rider's destination, leaves the rider satisfied,
    and requests another rider.

    === Attribute ===
    @param Rider rider: the rider who is picked up by driver.
    @param Driver driver: the driver who picks up rider.
    """

    def __init__(self, timestamp, driver, rider):
        """Initialize a Pickup event.

        @param Dropoff self: this Dropoff event.
        @param Driver driver: the driver who picks up rider.
        @param Rider rider: the rider who is picked up by driver.
        @rtype: None
        """
        super().__init__(timestamp)
        self.rider, self.driver = rider, driver

    def do(self, dispatcher, monitor):
        """The Driver finishes current ride, leaves the rider satisfied, and
        driver requests for another ride.
        Return a DriverRequest event.

        @param Dropoff self: this Dropoff event.
        @param  Dispatcher dispatcher: Dispatcher object.
        @param Monitor monitor: Monitor object
        @rtype: list[Event]

        >>> dispatcher = Dispatcher()
        >>> monitor = Monitor()
        >>> rider = Rider('a', Location(1, 2), Location(4, 5), 10)
        >>> driver = Driver('b', Location(4, 5), 2)
        >>> d = Dropoff(3, driver, rider)
        >>> result = d.do(dispatcher, monitor)
        >>> len(result)
        1
        >>> type(result[0])
        <class 'event.DriverRequest'>
        """
        self.driver.end_ride()
        events = []
        monitor.notify(self.timestamp, DRIVER, DROPOFF,
                       self.driver.id, self.driver.location)
        events.append(DriverRequest(self.timestamp, self.driver))
        self.driver.is_idle = True
        self.driver.destination = None
        return events

    def __str__(self):
        """Return a string representation of this event.

        @param Dropoff self: this Dropoff event.
        @rtype: str

        >>> r = Rider('a', Location(2, 2), Location(2, 3), 5)
        >>> d = Driver('b', Location(2, 3), 13)
        >>> drop_off = Dropoff(4, d, r)
        >>> print(drop_off)
        4 -- b: Dropoff a
        """
        return "{} -- {}: Dropoff {}".format(self.timestamp, self.driver,
                                             self.rider)


def create_event_list(filename):
    """Return a list of Events based on raw list of events in <filename>.

    Precondition: the file stored at <filename> is in the format specified
    by the assignment handout.

    @param filename: str
        The name of a file that contains the list of events.
    @rtype: list[Event]
    """
    # This method needs to open a file so no doctest provided.
    events = []
    with open(filename, "r") as file:
        for line in file:
            line = line.strip()

            if not line or line.startswith("#"):
                # Skip lines that are blank or start with #.
                continue

            # Create a list of words in the line, e.g.
            # ['10', 'RiderRequest', 'Cerise', '4,2', '1,5', '15'].
            # Note that these are strings, and you'll need to convert some
            # of them to a different type.
            tokens = line.split()
            timestamp = int(tokens[0])
            event_type = tokens[1]

            # HINT: Use Location.deserialize to convert the location string to
            # a location.

            if event_type == "DriverRequest":
                # TODO
                # Create a DriverRequest event.
                driver_id = tokens[2]
                location = deserialize_location(tokens[3])
                speed = int(tokens[4])
                driver = Driver(driver_id, location, speed)
                event = DriverRequest(timestamp, driver)
                events.append(event)
            elif event_type == "RiderRequest":
                # TODO
                # Create a RiderRequest event.
                rider_id = tokens[2]
                origin = deserialize_location(tokens[3])
                destination = deserialize_location(tokens[4])
                patience = int(tokens[5])
                rider = Rider(rider_id, origin, destination, patience)
                event = RiderRequest(timestamp, rider)
                events.append(event)
    return events
