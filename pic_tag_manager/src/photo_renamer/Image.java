package photo_renamer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Image {
	
	public File image;
	
	public Image (String imagePath) {
		
		this.image = new File(imagePath);
	}

	public String getImage() {

		return image.getAbsolutePath();
		
	}
	
	
	/**
	 * Gets the tags that this images currently has.
	 * 
	 * @return ArrayList<String> containing the tags that this image currently has.
	 */
	public ArrayList<String> getTags() {
		
		String[] imageName = this.getName().split("@");
    	Integer i = imageName.length;
    	imageName[i - 1] = imageName[i - 1].substring(0, imageName[i - 1].lastIndexOf("."));
    	ArrayList<String> tagList = new ArrayList<String>();
    	for (int a = 1; a < imageName.length; a++) {
    		tagList.add("@" + imageName[a]);
    	}
    	return tagList;
	}
	
	
	/**
     * Adds the tag from given this image.
     * 
     * @param tag
     * 			 the tag to be added.
     * 
     * @param tagLogger
     * 			 the object to record the change of tags.
     * 
     * @param nameLogger 
     * 			the object to record the change of image names.
     * @throws IOException if the file doesn't exist
     */
    public void addTag(String tag) throws IOException {
    	if (!this.getName().contains(tag)){
    		String imagePath = this.getImage();
        	String newPath = imagePath.substring(0, imagePath.lastIndexOf(".")) +
        			         tag + imagePath.substring(imagePath.lastIndexOf("."));
        	String newName = this.getName().substring(0, this.getName().lastIndexOf(".")) + 
        					 tag + this.getName().substring(this.getName().lastIndexOf("."));
        	NameLogger.getInstance().upDate(this.getName(), newName);
        	this.image.renameTo(new File(newPath));
        	this.image = new File(newPath);
        	TagLogger.getInstance().addTagToRecord(tag);
    	}
    }
    
    
    /**
     * Removes the tag from given this image.
     * 
     * @param tag 
     * 			the tag to be removed.
     * 
     * @param tagLogger
     * 		    the object to record the change of tags.
     *
     * @param nameLogger 
     * 			the object to record the change of image names.
     * @throws IOException if the file doesn't exist
     */
    public void removeTag(String tag) throws IOException {
    	String imagePath = this.getImage();
    	if (imagePath.contains(tag)){
    		String newPath = imagePath.replaceFirst(tag, "");
    		String newName = this.getName().replaceFirst(tag, "");
    		NameLogger.getInstance().upDate(this.getName(), newName);
    		this.image.renameTo(new File(newPath));
    		this.image = new File(newPath);
    	}
    	TagLogger.getInstance().removeTagFromRecord(tag);
    }
    
    
    /**
     * Gets history names of this image.
     *
     * @param nameLogger 
     * 				the object to record the change of image names.
     * @throws IOException
     * @return the ArrayList<String> containing the history names.
     */
    public ArrayList<String> showHistoryName() {
    	ArrayList<String> result = new ArrayList<String>();
    	String currentName = this.getName();
    	for (String timeStamp: NameLogger.getInstance().getRecorder().descendingKeySet()){
    		if (NameLogger.getInstance().getRecorder().get(timeStamp)[1].equals(currentName)) {
    			currentName = NameLogger.getInstance().getRecorder().get(timeStamp)[0];
    			if (!result.contains(NameLogger.getInstance().getRecorder().get(timeStamp)[0])){
    				if (!this.getName().equals(NameLogger.getInstance().getRecorder().get(timeStamp)[0])) {
    					result.add(0, NameLogger.getInstance().getRecorder().get(timeStamp)[0]);
    				}
    			}
    		}
    	}
//    	result.remove(this.getName());
    	return result;
    }
    
    
    /**
     * Gets name history of this image. This is a clear String representation.
     * 
     * @param nameLogger 
     * 			the object to record the change of image names.
     * @throws IOException
     * @return the Stirng representing the history.
     */
    public String findHistory() {
    	String result = this.getName();
    	String currentName = this.getName();
    	for (String timeStamp: NameLogger.getInstance().getRecorder().descendingKeySet()){
    		if (NameLogger.getInstance().getRecorder().get(timeStamp)[1].equals(currentName)) {
    			result = NameLogger.getInstance().getRecorder().get(timeStamp)[0] + "\n" 
    			+ "At time: " + timeStamp + " Change to: " + result;
    			currentName = NameLogger.getInstance().getRecorder().get(timeStamp)[0];
    		}
    	}
    	return result;
    }
    
    
    /**
     * Reverts the name of this image to a given oldName
     * 
     * @param oldName 
     * 				The name being reverted to.
     * 
     * @param tagLogger 
     * 				the object to record the change of tags.
     * 
     * @param nameLogger 
     * 				the object to record the change of image names.
     * @throws IOException if the file doesn't exist
     */
    public void revertHistoryName(String oldName) throws IOException {
    	String newPath = this.getImage().replace(this.getName(), oldName);
    	String currName = this.getName();
    	this.image.renameTo(new File(newPath));
    	this.image = new File(newPath);
    	
    	String[] tagCurrName = currName.split("@");
    	Integer i = tagCurrName.length;
    	tagCurrName[i - 1] = tagCurrName[i - 1].substring(0,tagCurrName[i - 1].lastIndexOf("."));
    	
    	String[] tagOldName = oldName.split("@");
    	i = tagOldName.length;
    	tagOldName[i - 1] = tagOldName[i - 1].substring(0,tagOldName[i - 1].lastIndexOf("."));
    	
    	List<String> tagCurrNameList = Arrays.asList(tagCurrName);
    
    	List<String> tagOldNameList = Arrays.asList(tagOldName);
    	for (String currTag: tagCurrNameList) {
    		if (!tagOldNameList.contains(currTag)) {
    			TagLogger.getInstance().removeTagFromRecord("@" + currTag);
    		}
    	}
    	for (String oldTag: tagOldNameList) {
    		if (!tagCurrNameList.contains(oldTag)) {
    			TagLogger.getInstance().addTagToRecord("@" + oldTag);
    		}
    	}
    	NameLogger.getInstance().upDate(currName, oldName);
    }
    
    
    /**
     * Gets the name of this image.
     * 
     * @return the name of this image.
     */
    public String getName() {
		return image.getName();
	}
    
    
//	/**
//	 * This method is for code that tests this class.
//	 * @param args the command line args.
//	 * @throws ClassNotFoundException if filePath is not a valid path
//	 * @throws IOException if the file doesn't exist 
//	 */
//    public static void main(String[] args) throws ClassNotFoundException, IOException {
//    	
//
//    	NameLogger nameLogger = new NameLogger();
//    	TagLogger tagLogger = new TagLogger();
//    	Image image2 = new Image("D:/pictures/Totti.jpg");
//    	image2.addTag("@Roma", tagLogger, nameLogger);
//    	image2.addTag("@King", tagLogger, nameLogger);
//    	image2.removeTag("@Roma", tagLogger, nameLogger);
//    	System.out.println(tagLogger.getMasterList());
//    	System.out.println(nameLogger.toString());
//    	
//    }

}