package photo_renamer;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.AbstractButton;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;



public class PhotoRenamer {
	
	public static void main(String[] args) {
		JFrame jf = new JFrame("Photo Renamer");
		jf.setSize(300, 400);
		
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		
		JButton addTagButton = new JButton("Add Tag");
		JButton removeTagButton = new JButton("Remove Tag");
		JButton revertNameButton = new JButton("Revert Tag");
		addTagButton.setEnabled(false);
		removeTagButton.setEnabled(false);
		revertNameButton.setEnabled(false);
		
		JLabel directoryLabel = new JLabel("Select a directory");
		JPanel imagePanel = new JPanel();
		jf.add(imagePanel, BorderLayout.EAST);
		JTextArea textArea = new JTextArea("No image selected");
		imagePanel.add(textArea);

		
		DefaultListModel<String> listModel = new DefaultListModel<String>();
		JList<String> list = new JList<String>(listModel);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.ensureIndexIsVisible(1);
		list.setVisibleRowCount(10);
		list.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				if (list.isSelectionEmpty()) {
					addTagButton.setEnabled(false);
					removeTagButton.setEnabled(false);
					revertNameButton.setEnabled(false);
					imagePanel.removeAll();
					imagePanel.add(textArea, BorderLayout.NORTH);
					jf.add(imagePanel, BorderLayout.EAST);
					jf.setVisible(true);
				} else if (list.getSelectedValue().equals("No Image exists under selected directory.")){
					addTagButton.setEnabled(false);
					removeTagButton.setEnabled(false);
					revertNameButton.setEnabled(false);
				} else {
					addTagButton.setEnabled(true);
					removeTagButton.setEnabled(true);
					revertNameButton.setEnabled(true);
					imagePanel.removeAll();
					imagePanel.repaint();
					
					File file = new File(directoryLabel.getText());
					FileNode fileTree = new FileNode(file.getName(), null, FileType.DIRECTORY);
					FileNode.buildTree(file, fileTree);
					BufferedImage img = null;
					System.out.println(list.getSelectedValue());
					System.out.println(fileTree.getChildren());
					System.out.println(fileTree.findChild(list.getSelectedValue()));
					try {
						
						img = ImageIO.read(new File(fileTree.findChildPath(list.getSelectedValue())));
					} catch (IOException e) {

					}		
					ImageIcon icon = new ImageIcon(img);
					icon.setImage(icon.getImage().getScaledInstance(300, (int) ((300.0/icon.getIconWidth())*icon.getIconHeight()), Image.SCALE_DEFAULT));
					JLabel imageLabel = new JLabel(null, icon, JLabel.CENTER);
					imagePanel.add(imageLabel,BorderLayout.CENTER);
					
					jf.add(imagePanel,BorderLayout.EAST);
					jf.pack();
					jf.setVisible(true);
				}
				
			}
			
		});
		//list.addListSelectionListener(ImageSelectionListener);
		JScrollPane listScrollPane = new JScrollPane(list);
		listScrollPane.setPreferredSize(new Dimension(200, 250));
		jf.add(listScrollPane, BorderLayout.WEST);
		



		
		jf.add(directoryLabel, BorderLayout.PAGE_START);
		
		
		JPanel buttonPanel = new JPanel();
		JButton cd = new JButton("Choose Directory");
		cd.setVerticalTextPosition(AbstractButton.CENTER);
		cd.setHorizontalTextPosition(AbstractButton.LEADING);
		cd.setMnemonic(KeyEvent.VK_D);
		cd.setActionCommand("disable");
		ActionListener cdbuttonListener = new FileChooserButtonListener(jf, directoryLabel,
				fileChooser, listModel, list);
		cd.addActionListener(cdbuttonListener);
		buttonPanel.add(cd);
		

		addTagButton.setVerticalTextPosition(AbstractButton.CENTER);
		addTagButton.setHorizontalTextPosition(AbstractButton.LEADING);
		addTagButton.setMnemonic(KeyEvent.VK_D);
		addTagButton.setActionCommand("disable");
		ActionListener addbuttonListener = new AddTagButtonListener(directoryLabel, listModel, list);
		addTagButton.addActionListener(addbuttonListener);
		

		removeTagButton.setVerticalTextPosition(AbstractButton.CENTER);
		removeTagButton.setHorizontalTextPosition(AbstractButton.LEADING);
		removeTagButton.setMnemonic(KeyEvent.VK_D);
		removeTagButton.setActionCommand("disable");
		ActionListener removebuttonListener = new RemoveTagButtonListener(directoryLabel, listModel, list);
		removeTagButton.addActionListener(removebuttonListener);
		

		revertNameButton.setVerticalTextPosition(AbstractButton.CENTER);
		revertNameButton.setHorizontalTextPosition(AbstractButton.LEADING);
		revertNameButton.setMnemonic(KeyEvent.VK_D);
		revertNameButton.setActionCommand("disable");
		ActionListener revertbuttonListener = new RevertImageButtonListener(directoryLabel, listModel, list);
		revertNameButton.addActionListener(revertbuttonListener);
		
		
		buttonPanel.add(addTagButton);
		buttonPanel.add(removeTagButton);
		buttonPanel.add(revertNameButton);
		
		jf.add(buttonPanel, BorderLayout.SOUTH);
		
		
		jf.pack();
		jf.setVisible(true);

	}
	
}
