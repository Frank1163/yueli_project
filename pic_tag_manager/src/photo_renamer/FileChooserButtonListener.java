package photo_renamer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;


public class FileChooserButtonListener implements ActionListener {
	/** The window the button is in. */
	private JFrame directoryFrame;
	/** The label for the full path to the chosen directory. */
	private JLabel directoryLabel;
	/** The file chooser to use when the user clicks. */
	private JFileChooser fileChooser;
	/** The area to use to display the nested directory contents. */
	private DefaultListModel<String> listModel;
	private JList<String> list;
	
	/**
	 * An action listener for window dirFrame, displaying a file path on
	 * dirLabel, using fileChooser to choose a file.
	 *
	 * @param dirFrame
	 *            the main window
	 * @param dirLabel
	 *            the label for the directory path
	 * @param fileChooser
	 *            the file chooser to use
	 */
	public FileChooserButtonListener(JFrame dirFrame, JLabel dirLabel,JFileChooser fileChooser,
									DefaultListModel<String> listModel, JList<String> list) {
		this.directoryFrame = dirFrame;
		this.directoryLabel = dirLabel;
		this.fileChooser = fileChooser;
		this.listModel = listModel;
		this.list = list;
	}
	
	/**
	 * Handle the user clicking on the open button.
	 *
	 * @param e
	 *            the event object
	 */
	@Override
	public void actionPerformed(ActionEvent e){
		listModel.clear();
		int returnVal = fileChooser.showOpenDialog(directoryFrame.getContentPane());
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fileChooser.getSelectedFile();
			if (file.exists()) {
				directoryLabel.setText(file.getAbsolutePath());
				FileNode fileTree = new FileNode(file.getName(), null, FileType.DIRECTORY);
				FileNode.buildTree(file, fileTree);
				FileNode.buildDirectoryContents(fileTree, listModel);
				list.setSelectedIndex(0);
			}
		}
		if (listModel.isEmpty()) {
			listModel.addElement("No Image exists under selected directory.");
		}
	}
}
