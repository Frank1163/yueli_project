package photo_renamer;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import javax.swing.AbstractButton;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class RevertImageButtonListener implements ActionListener {

	private JLabel directoryLabel;
	private DefaultListModel<String> listModel;
	private Image image;
	private JList<String> list;
	
	public RevertImageButtonListener(JLabel directoryLabel, DefaultListModel<String> listModel, JList<String> list) {
		this.directoryLabel = directoryLabel;
		this.listModel = listModel;
		this.list = list;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		String[] names = directoryLabel.getText().split(" ");
		File file = new File(names[2]);
		FileNode fileTree = new FileNode(file.getName(), null, FileType.DIRECTORY);
		FileNode.buildTree(file, fileTree);
		image = new Image(fileTree.findChildPath(listModel.getElementAt(list.getSelectedIndex())));
		
		if (image.showHistoryName().size() < 1) {
			JOptionPane.showMessageDialog(null, "This image doesn't have any history name yet.");
		} else {
			JFrame photoRenamerRevert = new JFrame("Revert name");
			JLabel imageLabel = new JLabel(image.getName());
			photoRenamerRevert.add(imageLabel, BorderLayout.PAGE_START);
			

			JTextArea history = new JTextArea();
			history.setText(image.findHistory());
			history.setEditable(false);
			JScrollPane textPane = new JScrollPane(history);
			JPanel textPanel = new JPanel();
			textPanel.setLayout(new BoxLayout(textPanel, BoxLayout.Y_AXIS));
			JLabel historyLabel = new JLabel("History record:");
			textPanel.add(historyLabel);
			textPanel.add(textPane);
			
			JPanel radioButtonPanel = new JPanel();
			ButtonGroup group = new ButtonGroup();
			JRadioButton radio;
			Box boxVertical = Box.createVerticalBox();
			for (String oldName: image.showHistoryName()) {
				radio = new JRadioButton(oldName);
				radio.setActionCommand(oldName);
				boxVertical.add(radio);
				group.add(radio);
			}
			radioButtonPanel.add(boxVertical);
			
			group.setSelected(group.getElements().nextElement().getModel(), true);

			RevertListener revertListener = new RevertListener(group, image, listModel, list, file, photoRenamerRevert);
			
			JButton revertButton = new JButton("Revert");
			revertButton.setVerticalTextPosition(AbstractButton.CENTER);
			revertButton.setHorizontalTextPosition(AbstractButton.LEADING);
			revertButton.setMnemonic(KeyEvent.VK_D);
			revertButton.setActionCommand("disable");
			revertButton.addActionListener(revertListener);
			
			JButton cancelButton = new JButton("Cancel");
			cancelButton.setVerticalTextPosition(AbstractButton.CENTER);
			cancelButton.setHorizontalTextPosition(AbstractButton.LEADING);
			cancelButton.setMnemonic(KeyEvent.VK_D);
			cancelButton.setActionCommand("disable");
			cancelButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					photoRenamerRevert.dispose();				
				}
				
			});
			
			JPanel buttonPanel = new JPanel();
			buttonPanel.add(revertButton);
			
			photoRenamerRevert.add(radioButtonPanel, BorderLayout.EAST);
			photoRenamerRevert.add(buttonPanel, BorderLayout.SOUTH);
			photoRenamerRevert.add(textPanel, BorderLayout.WEST);
			
			photoRenamerRevert.pack();
			photoRenamerRevert.setLocationRelativeTo(null);
			
			photoRenamerRevert.setVisible(true);
		}

	}

}
