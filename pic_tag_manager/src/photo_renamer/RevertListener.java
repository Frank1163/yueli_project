package photo_renamer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;


public class RevertListener implements ActionListener{

	private ButtonGroup group;
	private Image image;
	private DefaultListModel<String> listModel;
	private JList<String> list;
	private File file;
	private JFrame photoRenamerRevert;
	
	public RevertListener(ButtonGroup group, Image image, DefaultListModel<String> listModel, JList<String> list, File file, JFrame photoRenamerRevert) {
		this.group = group;
		this.image = image;
		this.listModel = listModel;
		this.list = list;
		this.file = file;
		this.photoRenamerRevert = photoRenamerRevert;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			image.revertHistoryName(group.getSelection().getActionCommand());
		} catch (IOException e1) {
			System.out.println("IOexception while accessing the image file");
		}
		
		JOptionPane.showMessageDialog(null, "You have Succcessfully revert the image name");
		photoRenamerRevert.dispose();
		listModel.clear();
		FileNode fileTree = new FileNode(file.getName(), null, FileType.DIRECTORY);
		FileNode.buildTree(file, fileTree);
		FileNode.buildDirectoryContents(fileTree, listModel);
		list.setSelectedIndex(0);
	}

}
