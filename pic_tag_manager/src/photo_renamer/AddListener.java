package photo_renamer;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.DefaultListModel;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class AddListener implements ActionListener {
	
	private JTextField newTag;
	private JPanel checkBoxPanel;
	private Image image;
	private DefaultListModel<String> listModel;
	private JList<String> list;
	private File file;
	private JFrame photoRenamerAdd;

	
	public AddListener(JTextField newTag, JPanel checkBoxPanel, Image image, DefaultListModel<String> listModel, JList<String> list, 
			File file, JFrame photoRenamerAdd){
		this.newTag = newTag;
		this.checkBoxPanel = checkBoxPanel;
		this.image = image;
		this.listModel = listModel;
		this.list = list;
		this.file = file;
		this.photoRenamerAdd = photoRenamerAdd;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		Component[] tags = checkBoxPanel.getComponents();
		JCheckBox tag;
		for (int i = 0; i < tags.length; i++) {
			tag = (JCheckBox)tags[i];
			if (tag.isSelected()){
				try {
					image.addTag(tag.getText());
				} catch (IOException e1) {
					System.out.println("IOexception while accessing configuration file");
				}
			}
		}
		if (newTag.getDocument().getLength() > 0) {
			try {
				image.addTag("@" + newTag.getText());
			} catch (IOException e1) {
				System.out.println("IOexception while accessing configuration file");
			}
		}


		
		JOptionPane.showMessageDialog(null, "You have Succcessfully add tag(s)");
		photoRenamerAdd.dispose();
		listModel.clear();
		FileNode fileTree = new FileNode(file.getName(), null, FileType.DIRECTORY);
		FileNode.buildTree(file, fileTree);
		FileNode.buildDirectoryContents(fileTree, listModel);
		list.setSelectedIndex(0);
	}
	
	

}
