package photo_renamer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;



import java.util.Calendar;

/**
 * Desgin pattern "singleton pattern" is being used in the class NameLogger,
 * since this program needs only one nameLogger to keep track of all the rename actions,
 * we make sure that only one instance of NameLogger is instantiate, 
 * and this instance is accessible to class Image that requires it by method getInstance().
 */
public class NameLogger {
	
	 private static final Logger logger = Logger.getLogger(NameLogger.class.getName());
	 private static final Handler consoleHandler = new ConsoleHandler();
	 
	 private static final NameLogger instance;
	 
	 static {
		 NameLogger tmp = null;
		 try{ 
	   		tmp  = new NameLogger(); 
		 }
		 catch (IOException e) {
			 System.out.println("Cannot Find such a file!");
		 }
		 catch (ClassNotFoundException o) {
			 System.out.println("Cannot Find such a file!");
			 o.printStackTrace();
		 }
	   	instance = tmp;
	 }
	 
	  /** A mapping of timestamps ids to oldNames and newNames. */
	 private TreeMap<String, String[]> recorder;
	 
	 /** Path of file NameLogger.csv */
	 public String csvFilePath;
	 
	 
	 private NameLogger() throws ClassNotFoundException, IOException{
		
		this.recorder = new TreeMap<String, String[]>();
		logger.setLevel(Level.ALL);
        consoleHandler.setLevel(Level.ALL);
        logger.addHandler(consoleHandler);
        
        /** Get URL of current working place. */
        URL location = NameLogger.class.getProtectionDomain().getCodeSource().getLocation();
        String csvFilePath = location.getFile().replace("%20", " ") + "NameLogger.csv";
        this.csvFilePath = csvFilePath.substring(1);
        logger.addHandler(new FileHandler(location.getFile().replace("%20", " ").substring(1) + "NameLogger.log"));

        File file = new File(csvFilePath);
        if (file.exists()) {
            readFromCSVFile();
        } else {
            file.createNewFile();
        }
	}
	 
	 
	/**
	 * Returns the unique instance of NameLogger.		 
	 * @return TagLogger the NameLogger instance.
	 */	
	 public static NameLogger getInstance() {
		return instance;
		}
	
	/**
     * Populates the recorder of name history from the file at path csvFilePath.
     * 
     * @param csvFilePath 
     * 					the path of the data file  
     * 
     * @throws FileNotFoundException if filePath is not a valid path
     */
	private void readFromCSVFile() throws FileNotFoundException {
		// FileInputStream can be used for reading raw bytes, like an image. 
        try{
        	Scanner scanner = new Scanner(new FileInputStream(csvFilePath));
        	String[] record;
        	String[] pairOfTags;
        	while(scanner.hasNextLine()) {
        		record = scanner.nextLine().split(",");
        		pairOfTags = new String[]{record[1],record[2]};
        		recorder.put(record[0], pairOfTags);
        		}
        	scanner.close();
        } catch (FileNotFoundException f){
        	logger.log(Level.FINE, "Cannot find the path when writing CSV file.", f);
			System.out.println("Cannot find the path");
        }
    }
		
	
	/**
	 * Writes the contents of recorder map to csv file at file path
	 * 
	 * @throws IOException if filePath is not a valid path
	 */
	public void writeCSV() throws IOException{
	        FileWriter writer = new FileWriter(csvFilePath);
	        try{
	        	for (String timeStamp: this.recorder.keySet()) {
		        	writer.append(timeStamp);
		        	writer.append(",");
		        	writer.append(recorder.get(timeStamp)[0]);
		        	writer.append(",");
		        	writer.append(recorder.get(timeStamp)[1]);
		        	writer.append("\n");
		        }
	        } catch (IOException a) {
             	logger.log(Level.FINE, "Cannot find the path when writing CSV file.", a);
	        	System.out.println("Cannot find the path");	        	
	        } finally {
	        	try {
		        	writer.flush();
		        	writer.close();
	        	} catch (IOException b) {
	        		logger.log(Level.FINE, "Cannot flush or close fileWriter.", b);
	        		System.out.println("Error while flushing or closing fileWriter");
	        	}
	        }
	}
	
	
	/**
	 * Updates the changes to recorder map according to the oldname and newname, 
	 * and writes this change to csv file logging this change.
	 * 
	 * @param oldName 
	 * 				the name of this image before changing tags
	 * 
	 * @param newName 
	 * 				the name of this image after changing tags
	 * 
	 * @throws IOException If the file doesn't exist
	 */
	public void upDate(String oldName, String newName) throws IOException{
		Calendar calendar = Calendar.getInstance();
		java.util.Date now = calendar.getTime();
		java.sql.Timestamp timeStamp = new java.sql.Timestamp(now.getTime());
		String[] tagTransfer = new String[]{oldName, newName};
		this.recorder.put(timeStamp.toString(), tagTransfer);

        try{
        	writeCSV();
        	}
        catch (IOException e){
			logger.log(Level.FINE, "Cannot find the path when writing CSV file.", e);
			System.out.println("cannot find the path");
		}
	}
	
	
	/**
	 * Gets history of all the renaming actions that have been done by far
	 * 
	 * @return TreeMap<String, String[]> representing the history of renaming.
	 */
	public TreeMap<String, String[]> getRecorder() {
		return recorder;
	}
	
	
	
	@Override
	public String toString() {
		String result = "";
		for (String timeStamp: this.recorder.keySet()) {
			result += "At time " + timeStamp + ":  " + recorder.get(timeStamp)[0] + 
					  " ===> " + recorder.get(timeStamp)[1] + "\n";
		}
		return result;
	}
	
	
//	/**
//	 * This method is for code that tests this class.
//	 * 
//	 * @param args
//	 *            the command line args.
//	 * @throws IOException if the file doesn't exist 
//	 */
//	public static void main(String[] args) throws ClassNotFoundException, IOException {
//		Image image1 = new Image("D:/pictures/Vincent.jpg");
//		NameLogger nameLogger = new NameLogger();
//		TagLogger tagLogger = new TagLogger();
//		image1.addTag("@FF7", tagLogger, nameLogger);
//	}
	
}