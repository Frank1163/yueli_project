package photo_renamer;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class AddTagButtonListener implements ActionListener {
	
	private JLabel directoryLabel;
	private DefaultListModel<String> listModel;
	private Image image;
	private JList<String> list;
	
	public AddTagButtonListener(JLabel directoryLabel, DefaultListModel<String> listModel, JList<String> list) {
		this.directoryLabel = directoryLabel;
		this.listModel = listModel;
		this.list = list;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JFrame photoRenamerAdd = new JFrame("Add Tags");
		String[] names = directoryLabel.getText().split(" ");
		File file = new File(names[2]);
		FileNode fileTree = new FileNode(file.getName(), null, FileType.DIRECTORY);
		FileNode.buildTree(file, fileTree);
		image = new Image(fileTree.findChildPath(list.getSelectedValue()));
		
		JLabel imageLabel = new JLabel(image.getName());
		
		JPanel checkBoxPanel = new JPanel();
		checkBoxPanel.setLayout(new BoxLayout(checkBoxPanel, BoxLayout.Y_AXIS));
		for (String tag: TagLogger.getInstance().getCurrExistingTag()){
			if (!image.getTags().contains(tag)){
				checkBoxPanel.add(new JCheckBox(tag));	
			}
		}
		
		
		JPanel textPanel = new JPanel();
		textPanel.setLayout(new BoxLayout(textPanel, BoxLayout.Y_AXIS));
		JLabel newTagLabel = new JLabel("Input new tag: ");
		textPanel.add(newTagLabel);
		JTextField newTag = new JTextField(10);
		newTag.setPreferredSize(new Dimension(20,1));;
		textPanel.add(newTag);
		
		AddListener addListener = new AddListener(newTag, checkBoxPanel,image, listModel, list, file, photoRenamerAdd);
		
		JButton addButton = new JButton("Add");
		addButton.setVerticalTextPosition(AbstractButton.CENTER);
		addButton.setHorizontalTextPosition(AbstractButton.LEADING);
		addButton.setMnemonic(KeyEvent.VK_D);
		addButton.setActionCommand("disable");
		addButton.addActionListener(addListener);
		
		JButton cancelButton = new JButton("Cancel");
		cancelButton.setVerticalTextPosition(AbstractButton.CENTER);
		cancelButton.setHorizontalTextPosition(AbstractButton.LEADING);
		cancelButton.setMnemonic(KeyEvent.VK_D);
		cancelButton.setActionCommand("disable");
		cancelButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				photoRenamerAdd.dispose();				
			}
			
		});
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.add(addButton);
		buttonPanel.add(cancelButton);
		
		
		photoRenamerAdd.setSize(300, 300);
		photoRenamerAdd.setLocationRelativeTo(null);
		photoRenamerAdd.add(checkBoxPanel, BorderLayout.CENTER);
		photoRenamerAdd.add(textPanel,BorderLayout.EAST);
		photoRenamerAdd.add(buttonPanel, BorderLayout.SOUTH);
		photoRenamerAdd.add(imageLabel, BorderLayout.PAGE_START);
		
	
		photoRenamerAdd.setVisible(true);

	}	

}
