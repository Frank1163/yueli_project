package photo_renamer;

import java.util.Map;

import javax.activation.MimetypesFileTypeMap;
import javax.swing.DefaultListModel;


import java.io.File;
import java.util.Collection;
import java.util.HashMap;

/**
 * The root of a tree representing a directory structure.
 */
public class FileNode {

	/** The name of the file or directory this node represents. */
	private String name;
	/** Whether this node represents a file or a directory. */
	private FileType type;
	/** This node's parent. */
	private FileNode parent;
	private Image image;
	/**
	 * This node's children, mapped from the file names to the nodes. If type is
	 * FileType.FILE, this is null.
	 */
	private Map<String, FileNode> children;

	/**
	 * A node in this tree.
	 *
	 * @param name
	 *            the file
	 * @param parent
	 *            the parent node.
	 * @param type
	 *            file or directory
	 * @see buildFileTree
	 */
	public FileNode(String name, FileNode parent, FileType type) {
		
		this.name = name;
		this.parent = parent;
		this.type = type;
		if (type == FileType.DIRECTORY) {
			this.children = new HashMap<String, FileNode>();
		}
		else {
			this.children = null;
		}
	}
	
	public FileNode(String name, FileNode parent, FileType type, Image image) {
		
		this.name = name;
		this.parent = parent;
		this.type = type;
		if (type == FileType.DIRECTORY) {
			this.children = new HashMap<String, FileNode>();
		}
		else {
			this.children = null;
		}
		this.image = image;
	}

	/**
	 * Find and return a child node named name in this directory tree, or null
	 * if there is no such child node.
	 *
	 * @param name
	 *            the file name to search for
	 * @return the node named name
	 */
	public FileNode findChild(String name) {
		
		FileNode result = null;
		if (this.children == null) {
			System.out.println("doesn't have child");
			return result;
		}
		for (String n: this.children.keySet()) {
			System.out.println(n);
			System.out.println(n.equals(name));
			if (n.equals(name)) {
				result = this.children.get(n);
			}
		}
		// If we did not find the child we are looking for, search grandchildren.
		if (result == null) {
			for (String m: this.children.keySet()) {
				result = this.children.get(m).findChild(name);
				if (result != null) {
					return result;
				}
			}
		}
		return result;
	}
	
	/**
	 * Find and return a String path of a child in this directory tree, or ""
	 * if there is no such child node.
	 *
	 * @param name
	 *            the file name to search for
	 * @return the path of this child image.
	 */
	public String findChildPath(String name) {
		String path = new String("");
		FileNode child = this.findChild(name);
		if (child != null) {
			return child.getImage().getImage();
		}
		return path;
	}

	/**
	 * Return the name of the file or directory represented by this node.
	 *
	 * @return name of this Node
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Set the name of the current node
	 *
	 * @param name
	 *            of the file/directory
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Return the child nodes of this node.
	 *
	 * @return the child nodes directly underneath this node.
	 */
	public Collection<FileNode> getChildren() {
		return this.children.values();
	}

	/**
	 * Return this node's parent.
	 * 
	 * @return the parent
	 */
	public FileNode getParent() {
		return parent;
	}

	/**
	 * Set this node's parent to p.
	 * 
	 * @param p
	 *            the parent to set
	 */
	public void setParent(FileNode p) {
		this.parent = p;
	}
	
	/**
	 * Return this node's image.
	 * 
	 * @return the image
	 */
	public Image getImage() {
		return this.image;
	}

	/**
	 * Set this node's image to newImage.
	 * 
	 * @param newImage
	 *            the new Image to be set.
	 */
	public void setImage(Image newImage) {
		this.image = newImage;
	}

	/**
	 * Add childNode, representing a file or directory named name, as a child of
	 * this node.
	 * 
	 * @param name
	 *            the name of the file or directory
	 * @param childNode
	 *            the node to add as a child
	 */
	public void addChild(String name, FileNode childNode) {
		this.children.put(name, childNode);
	}

	/**
	 * Return whether this node represents a directory.
	 * 
	 * @return whether this node represents a directory.
	 */
	public boolean isDirectory() {
		return this.type == FileType.DIRECTORY;
	}
	
	
	
	/**
	 * Build the tree of nodes rooted at file in the file system; note curr is
	 * the FileNode corresponding to file, so this only adds nodes for children
	 * that are images of file to the tree. 
	 * 
	 * Precondition: file represents a directory.
	 * 
	 * @param file
	 *            the file or directory we are building
	 * @param curr
	 *            the node representing file
	 * @return the FileNode curr after being built.
	 */
	public static FileNode buildTree(File file, FileNode curr) {
		
		File [] subfile = file.listFiles();
		if (subfile != null) {
			for (File n: subfile) {
				if (n.isDirectory()) {
					FileNode newnode = new FileNode(n.getName(), curr, FileType.DIRECTORY);
					curr.addChild(n.getName(), newnode);
					buildTree(n, newnode);
		        }
				else {
					String mimetype = new MimetypesFileTypeMap().getContentType(n);
					String type = mimetype.split("/")[0];
					if(type.equals("image") | n.getName().endsWith(".bmp") | n.getName().endsWith(".png")) {
						curr.addChild(n.getName(), new FileNode(n.getName(), curr, FileType.FILE, new Image(n.getAbsolutePath())));
				    }
				}
			}
		}
		return curr;
	}
	
	
	
	/**
	 * Build a string buffer representation of the contents of the tree rooted
	 * at n that contains all the images
	 *
	 * @param fileNode
	 *            the root of the subtree.
	 * @return the stringbuffer representation of the filenode.
	 */
	public static void buildDirectoryContents(FileNode fileNode, DefaultListModel<String> listModel) {
		Collection<FileNode> children = fileNode.getChildren();
		for (FileNode file: children) {
			if (file.isDirectory()) {
				buildDirectoryContents(file, listModel);
			} else {
				listModel.addElement(file.getName());
			}
		}
	}
	
	
	/**
	 * Build a string buffer representation of the contents of the tree rooted
	 * at n that contains all the images
	 *
	 * @param fileNode
	 *            the root of the subtree.
	 * @return the stringbuffer representation of the filenode.
	 */
	public static StringBuffer showContents(FileNode fileNode) {
		
		StringBuffer contents = new StringBuffer();
		Collection<FileNode> children = fileNode.getChildren();
		for (FileNode n: children) {
			if (n.isDirectory()) {
				contents.append(showContents(n));
			}
			// If type of n is Image.
			else {
				contents.append(n.getName());
				contents.append("\n");
			}
		}
		return contents;
	}
	

}
