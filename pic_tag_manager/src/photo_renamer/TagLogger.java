package photo_renamer;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Desgin pattern "singleton pattern" is being used in the class TagLogger,
 * since this program needs only one tagLogger to keep track of all the changes in tags,
 * we make sure that only one instance of TagLogger is instantiate, 
 * and this instance is accessible to class Image that requires it .
 */

public class TagLogger {
	
	private static final Logger logger = Logger.getLogger(TagLogger.class.getName());
	private static final Handler consoleHandler = new ConsoleHandler();
	
	
	
	private static final TagLogger instance;
	
	static {
		TagLogger tmp = null;
		try {
			tmp = new TagLogger();
		}
		catch (IOException e) {
			System.out.println("Cannot Find such a file!");
		} catch (ClassNotFoundException o){
			o.printStackTrace();
		}
		instance = tmp;
	}
	
	
	 
	/** A mapping of tag ids to its number. */
	private Map<String, Integer> tagRecord;
	
	/** Path of file tagRecord.ser  */
	public String serFilePath;
	
	
	private TagLogger() throws ClassNotFoundException, IOException {
		
		this.tagRecord = new HashMap<String, Integer>();
		
		logger.setLevel(Level.ALL);
        consoleHandler.setLevel(Level.ALL);
        logger.addHandler(consoleHandler);
        
        /** Get URL of current working place. */
        URL location = TagLogger.class.getProtectionDomain().getCodeSource().getLocation();
        String serFilePath = location.getFile().replace("%20", " ") + "TagRecord.ser";
        serFilePath = serFilePath.substring(1);
        logger.addHandler(new FileHandler(location.getFile().replace("%20", " ").substring(1) + "TagRecord.log"));
        this.serFilePath = serFilePath;

        
        File file = new File(serFilePath);
        if (file.exists()) {
            readFromSERFile();
        } else {
            file.createNewFile();
        }	
	}
    
	
	
	
	/**
	 * Returns the unique instance of TagLogger.
	 * @return TagLogger the tagLogger instance.
	 */
	public static TagLogger getInstance() {
		return instance;
	}
	
	
	/**
     * Adds the tag to the tagRecord, if that tag already exits, then number of that tag increases by one.
     * 
     * @param tag 
     * 			the tag needs to add.
	 * 
	 * @throws IOException If the file doesn't exist
     */
    public void addTagToRecord(String tag) throws IOException{
    	if (tagRecord.containsKey(tag)) {
    		Integer number = tagRecord.get(tag);
    		Integer newNumber = number + 1;
    		tagRecord.replace(tag, newNumber);
    	}
    	else{
    		tagRecord.put(tag, 1);
    	}
    	
    	try{saveToFile();}
    	catch (IOException e){
			logger.log(Level.FINE, "Cannot find the path when writing ser file.", e);
			System.out.println("Cannot find the path");
		}
    }
    
    
    /**
     * Removes the tag from the tagRecord.
     * 
     * @param tag
     * 			 the tag needs to remove.
     * 
     * @throws IOException if the file doesn't exist 
     */
    public void removeTagFromRecord(String tag) throws IOException{
    	Integer number = tagRecord.get(tag);
    	Integer newNumber = number - 1;
    	if (newNumber == 0) {
    		tagRecord.replace(tag, 0);
    	} else {
        	tagRecord.replace(tag, newNumber);
    	}
    	
    	try{saveToFile();}
    	catch (IOException e){
			logger.log(Level.FINE, "Cannot find the path when writing CSV file.", e);
			System.out.println("Cannot find the path");
		}
    	
    }
    
    
    /**
     * Populates the records of tags from the file at path serFilePath.
     * 
     * @throws ClassNotFoundException if filePath is not a valid path
     */
    @SuppressWarnings("unchecked")
	private void readFromSERFile() throws ClassNotFoundException {
		try {
            InputStream file = new FileInputStream(serFilePath);
            InputStream buffer = new BufferedInputStream(file);
            ObjectInput input = new ObjectInputStream(buffer);

            //deserialize the Map
            tagRecord = (Map<String, Integer>)input.readObject();
            input.close();
        } 
        catch (IOException ex) {
        	logger.log(Level.FINE, "Cannot read from input.", ex);
        }   
		
	}
	
    
	/**
     * Writes the tagRecord to file at serFilePath.
     * 
     * @param serFilePath 
     * 				the file to write the records to
     * 
     * @throws IOException if the file doesn't exist 
     */
    public void saveToFile() throws IOException {

        OutputStream file = new FileOutputStream(serFilePath);
        OutputStream buffer = new BufferedOutputStream(file);
        ObjectOutput output = new ObjectOutputStream(buffer);

        // serialize the Map
        output.writeObject(tagRecord);
        output.close();
    }
     
    
    /**
     * Retrieves the set of all tags that have been used.
     * 
     * @return the Set<String> containing all tags that have been used.
     */
    public Set<String> getMasterList() {
    	return tagRecord.keySet();
    }
    
    
    /**
     * Retrieves the set of current-existing tags.
     * 
     * @return Set<String> containing the set of current-existing tags.
     */
    public Set<String> getCurrExistingTag() {
    	Set<String> tags = new HashSet<String>(); 
    	for (String tag: tagRecord.keySet()) {
    		if (tagRecord.get(tag) != 0) {
    			tags.add(tag);
    		}
    	}
    	return tags;
    }
    
    
    /**
     * Prints the set of currently-existing tags.
     */
    @Override
    public String toString() {
		
		return tagRecord.keySet().toString();
	}
    
    
//	/**
//	 * This method is for code that tests this class.
//	 * @param args the command line args.
//	 * @throws ClassNotFoundException if filePath is not a valid path
//	 * @throws IOException if the file doesn't exist 
//	 */
//    public static void main(String[] args) throws ClassNotFoundException, IOException {
//    	
//     	Image image = new Image("D:/pictures/Vincent.jpg");
//
////    	NameLogger nameLogger = NameLogger.getInstance();
////    	NameLogger nameLogger2 = NameLogger.getInstance();
////    	
////    	TagLogger tagLogger = TagLogger.getInstance();
////    	TagLogger tagLogger2 = TagLogger.getInstance();
//    	
//    	image.addTag("@FF7");
//    	image.addTag("@FF8");
//    	System.out.println(TagLogger.getInstance().toString());
//    	image.addTag("@FF9");
//    	System.out.println(TagLogger.getInstance().toString());
//    	image.removeTag("@FF9");
//    	
//
//    	Image image2 = new Image("D:/pictures/Torres.jpg");
//    	image2.addTag("@LVP");
//    	image2.addTag("@#9");
//    	
//    	System.out.println(TagLogger.getInstance().toString());
//    	image2.revertHistoryName("Torres.jpg");
//    	
//    	
//    	System.out.println(image2.showHistoryName());
//    	image.revertHistoryName("Vincent@FF7@FF8@FF9.jpg");
//    	System.out.println(image.findHistory(NameLogger.getInstance()));
//    	System.out.println(TagLogger.getInstance().getMasterList());
//        System.out.println(TagLogger.getInstance().getCurrExistingTag());
//    	System.out.println(image2.findHistory(NameLogger.getInstance()));
//        System.out.println(image.getTags());
//        
//    	System.out.println("=============================\n");
//    	System.out.println(TagLogger.getInstance().toString());
//    	System.out.println("=============================\n");
//    	System.out.println(NameLogger.getInstance().toString());
//    
//    }
}