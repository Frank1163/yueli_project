package photo_renamer;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.DefaultListModel;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class RemoveListener implements ActionListener{

	private JPanel checkBoxPanel;
	private Image image;
	private DefaultListModel<String> listModel;
	private JList<String> list;
	private File file;
	private JFrame photoRenamerRemove;
	
	public RemoveListener(JPanel checkBoxPanel, Image image, DefaultListModel<String> listModel, JList<String> list, File file, JFrame photoRenamerRemove) {
		this.checkBoxPanel = checkBoxPanel;
		this.image = image;
		this.listModel = listModel;
		this.list = list;
		this.file = file;
		this.photoRenamerRemove = photoRenamerRemove;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		Component[] tags = checkBoxPanel.getComponents();
		JCheckBox tag;
		for (int i = 0; i < tags.length; i++) {
			tag = (JCheckBox)tags[i];
			if (tag.isSelected()) {
				try {
					image.removeTag(tag.getText());
				} catch (IOException e) {
					System.out.println("IOexception while accessing configuration file");
				}
			}
		}
		JOptionPane.showMessageDialog(null, "You have Succcessfully remove tag(s)");
		photoRenamerRemove.dispose();
		listModel.clear();
		FileNode fileTree = new FileNode(file.getName(), null, FileType.DIRECTORY);
		FileNode.buildTree(file, fileTree);
		FileNode.buildDirectoryContents(fileTree, listModel);
		list.setSelectedIndex(0);
		
	}

}
