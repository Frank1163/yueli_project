package photo_renamer;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.swing.AbstractButton;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class RemoveTagButtonListener implements ActionListener {
	
	private JLabel directoryLabel;
	private DefaultListModel<String> listModel;
	private Image image;
	private JList<String> list;
	
	public RemoveTagButtonListener(JLabel directoryLabel, DefaultListModel<String> listModel, JList<String> list) {
		this.directoryLabel = directoryLabel;
		this.listModel = listModel;
		this.list = list;
	}
	
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		String[] names = directoryLabel.getText().split(" ");
		File file = new File(names[2]);
		FileNode fileTree = new FileNode(file.getName(), null, FileType.DIRECTORY);
		FileNode.buildTree(file, fileTree);
		image = new Image(fileTree.findChildPath(listModel.getElementAt(list.getSelectedIndex())));
		
		
		if (image.getTags().size() == 0){
			JOptionPane.showMessageDialog(null, "This image doesn't have any tag yet.");
		} else {
			JFrame photoRenamerRemove = new JFrame("Remove Tags");
			JLabel imageLabel = new JLabel(image.getName());
			photoRenamerRemove.add(imageLabel, BorderLayout.PAGE_START);
			JPanel checkBoxPanel = new JPanel();
			JCheckBox checkBox;

			for (String tag: image.getTags()) {
				checkBox = new JCheckBox(tag);
				checkBoxPanel.add(checkBox);
			}
			
			RemoveListener removeListener = new RemoveListener(checkBoxPanel, image, listModel, list, file, photoRenamerRemove);
			
			JButton removeButton = new JButton("Remove");
			removeButton.setVerticalTextPosition(AbstractButton.CENTER);
			removeButton.setHorizontalTextPosition(AbstractButton.LEADING);
			removeButton.setMnemonic(KeyEvent.VK_D);
			removeButton.setActionCommand("disable");
			removeButton.addActionListener(removeListener);
			
			JButton cancelButton = new JButton("Cancel");
			cancelButton.setVerticalTextPosition(AbstractButton.CENTER);
			cancelButton.setHorizontalTextPosition(AbstractButton.LEADING);
			cancelButton.setMnemonic(KeyEvent.VK_D);
			cancelButton.setActionCommand("disable");
			cancelButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					photoRenamerRemove.dispose();				
				}
				
			});
			
			JPanel buttonPanel = new JPanel();			
			buttonPanel.add(removeButton);
			buttonPanel.add(cancelButton);
			
			photoRenamerRemove.add(checkBoxPanel, BorderLayout.CENTER);
			photoRenamerRemove.add(buttonPanel, BorderLayout.SOUTH);
			
			photoRenamerRemove.setSize(300, 300);
			photoRenamerRemove.setLocationRelativeTo(null);
			
			photoRenamerRemove.setVisible(true);
			
		}

	}

}
