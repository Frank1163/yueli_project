drop schema if exists part1 cascade;
create schema part1;
set search_path to part1;

/*Values constraints are defined here*/
create domain WhatSkill as varchar(10) 
	not null
	check (value in ('SQL', 'Scheme', 'Python', 'R', 'LaTeX'));

create domain SkillLevel as smallint 
	not null
	check (value>=0 and value <=5);

create domain Importance as smallint 
	not null
	check (value>=0 and value <=5);

create domain Score as smallint 
	not null
    	check (value>=0 and value <=100);

create domain DegreeLevel as varchar(20) 
	not null
	check (value in ('certificate', 'undergraduate', 'professional', 'professional', 'doctoral'));

create table Posting(
	pID integer primary key,
	positon varchar(100) not null);

create table SkillReq(
    pID integer primary key,
	what WhatSkill,
    level SkillLevel,
    important Importance);

create table PostingQuestion(
	pID integer,
	qID integer,
    question varchar(100) not null,
	primary key (pID, qID));      /*Aditional constraint: (pID, qID) is primary key.*/

/* resume.dtd starts. */
/* One interviewer can have as most one resume in this design */
create table ResumeInfo(
    rID integer primary key,
	forename varchar(100) not null,
    surname varchar(100) not null,
	DOB Date not null,
	citizenship varchar(100) not null,
	address varchar(100) not null,
	telephone varchar(100) not null,
	email varchar(100) not null);

create table ResumeTitle(
    rID integer primary key,
	title varchar(100) not null);

create table ResumeSummary(
	rID integer primary key,
    summary varchar(100) not null);

create table EducationInfo( 
	rID integer,
    degreeName varchar(100),
	level DegreeLevel,
	institution varchar(100) not null,
	honours Boolean not null,
    primary key (rID, degreeName));

create table EduPeriod(
	rID integer,
    degreeName varchar(100),
    startDate Date not null,
	endDate Date not null,
	primary key (rID, degreeName));

create table EduMajor(
	rID integer,
    degreeName varchar(100),
    major varchar(100),
	primary key (rID, degreeName, major));

create table EduMinor(
	rID integer,
    degreeName varchar(100),
    minor varchar(100),
	primary key (rID, degreeName, minor));

create table PositionInfo(
	rID integer,
	title varchar(100),
    description varchar(100) not null,
    location varchar(100) not null,   /*location refers to attribute where in resume.dtd*/
    primary key (rID, title));

create table ExperiencePeriod(
	rID integer,
	title varchar(100),
    startDate Date,
	endDate Date,
    primary key (rID, title));

create table Skills(
	rID integer,
	skill WhatSkill,
    level SkillLevel,
    primary key (rID, skill));

/* interview.dtd starts. */
create table InterviewerName(
	sID integer primary key,
	forename varchar(100) not null,
	surname varchar(100) not null);

create table InterviewInfo(
	rID integer,
	pID integer not null,
	sID integer not null,
	date Date,
	time Time,
	location varchar(100),
    	/* one resume can be only interiewed in a particular time.*/
    	primary key(rID, date, time),
    	foreign key (rID) references ResumeInfo,
    	foreign key (pID) references Posting,
	foreign key (sID) references InterviewerName); 
	
create table Assesment(
	rID integer,
    	date Date,
	time Time,
    	techProficiency Score,
    	communication Score,
    	enthusiasm Score,
    	primary key(rID, date, time));

create table AssesmentCollegiality(
    rID integer,
    date Date,
	time Time,
    collegiality Score,
    primary key(rID, date, time));

/* Not pretty sure... */
create table AssesmentAnswer(
	rID integer,
	date Date,
	time Time,
	qID integer,
	answer varchar(100),
    primary key(rID, date, time, qID));

create table InterviewerTitle(
	sID integer primary key,
	title varchar(100) not null);

create table InterviewerHonorific(
	sID integer primary key,
    honorific varchar(100) not null);

