import java.sql.*;

// Remember that part of your mark is for doing as much in SQL (not Java) 
// as you can. At most you can justify using an array, or the more flexible
// ArrayList. Don't go crazy with it, though. You need it rarely if at all.
import java.util.ArrayList;

public class Assignment2 {

    // A connection to the database
    Connection connection;

    Assignment2() throws SQLException {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Connects to the database and sets the search path.
     * 
     * Establishes a connection to be used for this session, assigning it to the
     * instance variable 'connection'. In addition, sets the search path to
     * markus.
     * 
     * @param url
     *            the url for the database
     * @param username
     *            the username to be used to connect to the database
     * @param password
     *            the password to be used to connect to the database
     * @return true if connecting is successful, false otherwise
     */
    public boolean connectDB(String URL, String username, String password) {
        // Replace this return statement with an implementation of this method!
    	try {
			this.connection = DriverManager.getConnection(URL,username,password);
			String setpath = "set search_path to markus";
			Statement set_path = this.connection.createStatement();
			set_path.execute(setpath);
			set_path.close();
			return true;
		} catch (SQLException e) {
			return false;
		}
        
    }

    /**
     * Closes the database connection.
     * 
     * @return true if the closing was successful, false otherwise
     */
    public boolean disconnectDB() {
        // Replace this return statement with an implementation of this method!
    	try {
			this.connection.close();
			return this.connection.isClosed();
		} catch (SQLException e) {
			return false;
		}
    	
    }

    /**
     * Assigns a grader for a group for an assignment.
     * 
     * Returns false if the groupID does not exist in the AssignmentGroup table,
     * if some grader has already been assigned to the group, or if grader is
     * not either a TA or instructor.
     * 
     * @param groupID
     *            id of the group
     * @param grader
     *            username of the grader
     * @return true if the operation was successful, false otherwise
     */
    public boolean assignGrader(int groupID, String grader) {
        // Replace this return statement with an implementation of this method!
    	
    	try {
    		// STEP 1 : Check the existence of the groupid
    		Boolean group_exist = false, assigned = false, type_ta = false;
    		
    		String check_group = "SELECT * FROM assignmentgroup WHERE group_id=?";
    		PreparedStatement pStatement = this.connection.prepareStatement(check_group);
    		pStatement.setInt(1,groupID);
			ResultSet rs = pStatement.executeQuery();
			if(rs.next()){
				group_exist = true;
			}

			
			// STEP 2 : Check the whether already assigned
			String check_assign = "SELECT * FROM grader WHERE group_id=? AND username IS NOT null";
			pStatement = this.connection.prepareStatement(check_assign);
			pStatement.setInt(1,groupID);
			rs = pStatement.executeQuery();
			if(rs.next()){
				assigned = true;
			}

			
			// STEP 3 : Check the type of the grader
			String check_type = "SELECT * FROM markususer WHERE username=? AND (type='TA' OR type='instructor')";
			pStatement = this.connection.prepareStatement(check_type);
			pStatement.setString(1,grader);
			rs = pStatement.executeQuery();
			if(rs.next()){
				type_ta = true;
			}
			
			if( group_exist==false || assigned==true || type_ta==false)
				return false;
			
			String insert = "INSERT INTO grader VALUES(?,?)";
			pStatement = this.connection.prepareStatement(insert);
			pStatement.setInt(1,groupID);
			pStatement.setString(2,grader);
			pStatement.executeUpdate();
			
			rs.close();
			pStatement.close();
			
			return true;
		} catch (SQLException e) {
			return false;
		}
    }

    /**
     * Adds a member to a group for an assignment.
     * 
     * Records the fact that a new member is part of a group for an assignment.
     * Does nothing (but returns true) if the member is already declared to be
     * in the group.
     * 
     * Does nothing and returns false if any of these conditions hold: - the
     * group is already at capacity, - newMember is not a valid username or is
     * not a student, - there is no assignment with this assignment ID, or - the
     * group ID has not been declared for the assignment.
     * 
     * @param assignmentID
     *            id of the assignment
     * @param groupID
     *            id of the group to receive a new member
     * @param newMember
     *            username of the new member to be added to the group
     * @return true if the operation was successful, false otherwise
     */
    public boolean recordMember(int assignmentID, int groupID, String newMember) {
        // Replace this return statement with an implementation of this method!1
    	try {

            String dropview1 = "DROP VIEW IF EXISTS group_info CASCADE";
            PreparedStatement pStatement = this.connection.prepareStatement(dropview1);
            pStatement.executeUpdate();
			
            String dropview2 = "DROP VIEW IF EXISTS group_size_info1 CASCADE";
            pStatement = this.connection.prepareStatement(dropview2);
            pStatement.executeUpdate();

			String dropview3 = "DROP VIEW IF EXISTS group_size_info2 CASCADE";
    		pStatement = this.connection.prepareStatement(dropview3);
            pStatement.executeUpdate();
			
			String dropview4 = "DROP VIEW IF EXISTS group_size_info CASCADE";
            pStatement = this.connection.prepareStatement(dropview4);
			pStatement.executeUpdate();
    		

    		// STEP 1 : the member is already in the group, return true
    		
    		String view1 = "CREATE VIEW group_info AS SELECT * FROM membership NATURAL JOIN assignmentgroup";
    		pStatement = this.connection.prepareStatement(view1);
			pStatement.executeUpdate();
			
			String already_in_check = "SELECT * FROM group_info WHERE assignment_id=? AND group_id=? AND username=?";
			pStatement = this.connection.prepareStatement(already_in_check);
			pStatement.setInt(1,assignmentID);
			pStatement.setInt(2,groupID);
			pStatement.setString(3,newMember);
			ResultSet rs = pStatement.executeQuery();
			if(rs.next()){
				return true;
			}


			
			/* STEP 2: check those conditions
			 * - the group is already at capacity -- done
			 * - newMember is not a valid username or is not a student --done
			 * - there is no assignment with this assignment ID -- 
			 * - the group ID has not been declared for the assignment.
			 * 
			 */
			
			String view2 = "CREATE VIEW group_size_info1 AS SELECT group_id,COUNT(*) from membership GROUP BY group_id";
    		pStatement = this.connection.prepareStatement(view2);
			pStatement.executeUpdate();
			
			String view3 = "CREATE VIEW group_size_info2 AS SELECT assignment_id,group_max,group_id FROM assignment NATURAL JOIN assignmentgroup";
    		pStatement = this.connection.prepareStatement(view3);
			pStatement.executeUpdate();
			
			String view4 = "CREATE VIEW group_size_info AS SELECT * FROM group_size_info2 NATURAL JOIN group_size_info1";
    		pStatement = this.connection.prepareStatement(view4);
			pStatement.executeUpdate();
			
			
			String capability_allowed = "SELECT * FROM group_size_info WHERE assignment_id=? AND group_id=? AND count=group_max";
			pStatement = this.connection.prepareStatement(capability_allowed);
			pStatement.setInt(1,assignmentID);
			pStatement.setInt(2,groupID);
			rs = pStatement.executeQuery();
			if(rs.next()){
				return false;
			}
			
			
			String valid_user = "SELECT * FROM markususer WHERE username=? AND type='student'";
			pStatement = this.connection.prepareStatement(valid_user);
			pStatement.setString(1,newMember);
			rs = pStatement.executeQuery();
			if(!rs.next()){
				return false;
			}
			
			
			String valid_as = "SELECT * FROM assignment WHERE assignment_id=?";
			pStatement = this.connection.prepareStatement(valid_as);
			pStatement.setInt(1,assignmentID);
			rs = pStatement.executeQuery();
			if(!rs.next()){
				return false;
			}
			
			String valid_group = "SELECT * FROM assignmentgroup WHERE group_id=?";
			pStatement = this.connection.prepareStatement(valid_group);
			pStatement.setInt(1,groupID);
			rs = pStatement.executeQuery();
			if(!rs.next()){
				return false;
			}
			
			//String insert = "INSERT INTO membership VALUES('" + newMember + "'," + groupID + ")";
			String insert = "INSERT INTO membership VALUES(?,?)";
			pStatement = this.connection.prepareStatement(insert);
			pStatement.setString(1,newMember);
			pStatement.setInt(2,groupID);
			pStatement.executeUpdate();
			
			
			
			rs.close();
			pStatement.close();
			
			return true;
		} catch (SQLException e) {
			return false;
		}
    }

    /**
     * Creates student groups for an assignment.
     * 
     * Finds all students who are defined in the Users table and puts each of
     * them into a group for the assignment. Suppose there are n. Each group
     * will be of the maximum size allowed for the assignment (call that k),
     * except for possibly one group of smaller size if n is not divisible by k.
     * Note that k may be as low as 1.
     * 
     * The choice of which students to put together is based on their grades on
     * another assignment, as recorded in table Results. Starting from the
     * highest grade on that other assignment, the top k students go into one
     * group, then the next k students go into the next, and so on. The last n %
     * k students form a smaller group.
     * 
     * In the extreme case that there are no students, does nothing and returns
     * true.
     * 
     * Students with no grade recorded for the other assignment come at the
     * bottom of the list, after students who received zero. When there is a tie
     * for grade (or non-grade) on the other assignment, takes students in order
     * by username, using alphabetical order from A to Z.
     * 
     * When a group is created, its group ID is generated automatically because
     * the group_id attribute of table AssignmentGroup is of type SERIAL. The
     * value of attribute repo is repoPrefix + "/group_" + group_id
     * 
     * Does nothing and returns false if there is no assignment with ID
     * assignmentToGroup or no assignment with ID otherAssignment, or if any
     * group has already been defined for this assignment.
     * 
     * @param assignmentToGroup
     *            the assignment ID of the assignment for which groups are to be
     *            created
     * @param otherAssignment
     *            the assignment ID of the other assignment on which the
     *            grouping is to be based
     * @param repoPrefix
     *            the prefix of the URL for the group's repository
     * @return true if successful and false otherwise
     */
    public boolean createGroups(int assignmentToGroup, int otherAssignment,
            String repoPrefix) {
        try {
            
            // STEP 1 : SET THE SERIAL NUMBER
            int serial = 0;
            String now_serial = "SELECT MAX(group_id) FROM assignmentgroup";
            PreparedStatement pStatement = this.connection.prepareStatement(now_serial);
            ResultSet rs = pStatement.executeQuery();
            if(rs.next()){
                serial = rs.getInt(1);
            }
            if(serial > 0){
                String set_serial = "SELECT setval('assignmentgroup_group_id_seq',?)";
                pStatement = this.connection.prepareStatement(set_serial);
                pStatement.setInt(1,serial);
                rs = pStatement.executeQuery();
            }
            
            


            // find all student who are defined in the markus user
            String dropview1 = "DROP VIEW IF EXISTS all_students CASCADE";
            pStatement = this.connection.prepareStatement(dropview1);
            pStatement.executeUpdate();

            String view1 = "CREATE VIEW all_students AS SELECT username FROM markususer WHERE type='student'";
            pStatement = this.connection.prepareStatement(view1);
            pStatement.executeUpdate();


            // check whether no valid students
            String num_stu = "SELECT COUNT(*) FROM all_students";
            int student_count = 0;
            pStatement = this.connection.prepareStatement(num_stu);
            rs = pStatement.executeQuery();
            if(!rs.next())
                return true;
            else
                student_count = rs.getInt(1);

            // get max_group_size for this assignment
            int size = 0;
            String group_size_info = "SELECT group_max FROM assignment WHERE assignment_id=?";
            pStatement = this.connection.prepareStatement(group_size_info);
            pStatement.setInt(1,assignmentToGroup);
            rs = pStatement.executeQuery();
            if(rs.next())
                size = rs.getInt(1);
            else
                return false;
            


            // gather information of other assignment

            String dropview2 = "DROP VIEW IF EXISTS other_asinfo1 CASCADE";
            pStatement = this.connection.prepareStatement(dropview2);
            pStatement.executeUpdate();


            // consider 1. no membership for some group
            //          2. no result for some group
            String view2 = "CREATE VIEW other_asinfo1 AS ";
            view2 += "SELECT assignment_id ";
            view2 += "FROM assignment ";
            view2 += "WHERE assignment_id=" + otherAssignment;
            pStatement = this.connection.prepareStatement(view2);
            pStatement.executeUpdate();



            String dropview3 = "DROP VIEW IF EXISTS other_asinfo2 CASCADE";
            pStatement = this.connection.prepareStatement(dropview3);
            pStatement.executeUpdate();


            String view3 = "CREATE VIEW other_asinfo2 AS ";
            view3 += "SELECT * ";
            view3 += "FROM other_asinfo1 , all_students WHERE username IS NOT NULL";
            pStatement = this.connection.prepareStatement(view3);
            pStatement.executeUpdate();



            String dropview4 = "DROP VIEW IF EXISTS other_asinfo3 CASCADE";
            pStatement = this.connection.prepareStatement(dropview4);
            pStatement.executeUpdate();


            String view4 = "CREATE VIEW other_asinfo3 AS ";
            view4 += "SELECT * ";
            view4 += "FROM membership NATURAL FULL JOIN assignmentgroup WHERE assignment_id=" + otherAssignment;
            pStatement = this.connection.prepareStatement(view4);
            pStatement.executeUpdate();






            String dropview5 = "DROP VIEW IF EXISTS other_asinfo4 CASCADE";
            pStatement = this.connection.prepareStatement(dropview5);
            pStatement.executeUpdate();


            String view5 = "CREATE VIEW other_asinfo4 AS ";
            view5 += "SELECT * ";
            view5 += "FROM other_asinfo3 NATURAL FULL JOIN other_asinfo2";
            pStatement = this.connection.prepareStatement(view5);
            pStatement.executeUpdate();



            


            String dropview6 = "DROP VIEW IF EXISTS other_asinfo CASCADE";
            pStatement = this.connection.prepareStatement(dropview6);
            pStatement.executeUpdate();


            String view6 = "CREATE VIEW other_asinfo AS ";
            view6 += "SELECT username,assignment_id,mark ";
            view6 += "FROM other_asinfo4 ";
            view6 += "NATURAL LEFT JOIN result ";
            view6 += "ORDER BY mark DESC NULLS LAST, username ASC";
            pStatement = this.connection.prepareStatement(view6);
            pStatement.executeUpdate();


            // initialize groups in the assignmentgroup
            int times = student_count / size;
            if(student_count % size != 0)
                times++;



            while(times > 0){
                String url = repoPrefix + "/group_" + (serial + 1);
                serial++;
                String initialize_group = "INSERT INTO assignmentgroup(assignment_id,repo) VALUES(?,?)";
                pStatement = this.connection.prepareStatement(initialize_group);
                pStatement.setInt(1,assignmentToGroup);
                pStatement.setString(2,url);
                pStatement.executeUpdate();
                times--;
            }



            // create group and assign students
            
            PreparedStatement p2 = this.connection.prepareStatement("SELECT username FROM other_asinfo");
            ResultSet rs2 = p2.executeQuery();
            
            while(rs2.next()){
                String cur_user = rs2.getString(1);

                String group_id_get = "SELECT group_id FROM assignmentgroup WHERE assignment_id=?";
                pStatement = this.connection.prepareStatement(group_id_get);
                pStatement.setInt(1,assignmentToGroup);
                rs = pStatement.executeQuery();

                while(rs.next()){
                    int cur_group = rs.getInt(1);
                    if(recordMember(assignmentToGroup,cur_group,cur_user))
                        break;
                }
            }


            rs2.close();
            p2.close();
            
            rs.close();
            pStatement.close();
            
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void main(String[] args) {
        // You can put testing code in here. It will not affect our autotester.
    }
}
